/* ******************************************************
**  Module générique pour faire un "populate()" sans filtre *
** ******************************************************/
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

/* GET users listing. */
router.get('/', function (req, res, next) {
    if ((req.session.passport) && (req.session.passport.user !== null)) {
        Minismall.schemas[req.message.modelName].find({}).populate(req.message.pop_ref)
            .exec(function (err, result) {
                if (err) {
                    console.log("error: ", err);
                    return handleError(err);
                } else {
                    console.log(result);
                    if (result.length === 0) result = null;
                    if (req.message.return_type === null) {
                        res.render(req.message.view, {
                            title: req.message.title,
                            data: result
                        });
                    } else {
                        res.send(JSON.stringify(result));
                    }
                }
            }
        );
    } else {
        res.redirect('/');  // affichage boîte de login si pas authentifié
    }
});

module.exports = router;