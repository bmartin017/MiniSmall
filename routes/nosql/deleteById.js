/* *********************************************************
**  Module générique pour faire un "deleteOne()" via l’_id *
** *********************************************************/
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

/* DELETE record from _id into url and into config_actions.json */
router.route('/:_id').get(function (req, res) {
    if ((req.session.passport) && (req.session.passport.user !== null)) {
        Minismall.schemas[req.message.modelName].deleteOne({ _id: new ObjectId(req.params._id) }, function (err, result) {
            if (err) {
                throw err;
            }
            Minismall.schemas[req.message.modelName].find({}, function (err, result2) {
                console.log("result after deleteById : ", result2);
                res.render(req.message.view, {
                    title: req.message.title,
                    data: result2
                });
            });
        });
    } else {
        res.redirect('/');
    }
});
module.exports = router;