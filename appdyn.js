const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require("fs");
const session = require('express-session');
const passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;
const multer = require('multer');
require('dotenv').config();

global.Minismall = {}

Minismall.upload = multer({
    dest: './public/data/uploads/'
})

/* Chargement du fichier de configuration générale du Framework MiniSmall */
Minismall.config = JSON.parse(fs.readFileSync("./config_minismall.json", "utf8"));

/*chargement de la configuration JSON des actions et watchdog sur le fichier pour rechargement à chaud */
Minismall.actions_json = JSON.parse(fs.readFileSync("./routes/config_actions.json", "utf8"));
const filePath = "./routes/config_actions.json";
fs.watch(filePath, "utf-8", function(event, trigger) {
    console.log("-- The file config_actons.json has changed ! --");
    Minismall.actions_json = JSON.parse(fs.readFileSync(filePath, "utf8"));
    console.log("-- The file config_actons.json reloaded ! --");
});

const hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials', function() {
    console.log('partials registered');
});

hbs.registerHelper('compare', function(lvalue, rvalue, options) {
    //console.log("####### COMPARE lvalue :", lvalue, " et rvalue: ", rvalue);
    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
    const operator = options.hash.operator || "==";
    const operators = {
        '==': function (l, r) {
            return l == r;
        },
        '===': function (l, r) {
            return l === r;
        }
    };
    if (!operators[operator])
        throw new Error("'compare' doesn't know the operator " + operator);
    const result = operators[operator](lvalue, rvalue);
    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
if (Minismall.config.mongodb.used) {
    Minismall.db = {};
    const mongoClient = require('mongodb').MongoClient;
    // Connexion URL
    const url = process.env.MONGODB_URI;
    // Utilisation de la methode “connect” pour se connecter au serveur
    mongoClient.connect(url, {
        useUnifiedTopology: true
    }, function(err, client) {
        Minismall.db = client.db('gretajs'); //On met en global la connexion à la base
        console.log("Connected successfully to server: global.db initialized");
    });
}
if (Minismall.config.mongoose.used) {
    // connexion depuis mongoose
    Minismall.schemas = {};
    const mongoose = require('mongoose');
    mongoose.connect(process.env.MONGOOSE_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function(err) {
        if (err) {
            throw err;
        } else console.log('Connected Mongoose');
    });

    // chargement des schémas depuis le fichier de configuration JSON dans une variable
    const database_schemas = JSON.parse(fs.readFileSync("database_schema.json", 'utf8'));
    // Initialisation de chaque schéma par association entre le schéma et la collection
    for (let modelName in database_schemas) {
        Minismall.schemas[modelName] = mongoose.model(modelName, database_schemas[modelName].schema,
            database_schemas[modelName].collection);
    }
}

if (Minismall.config.sequelize.used) {
    // connexion à mariadb via Sequelize
    const Sequelize = require("sequelize");

    // configuration des paramètres de la connexion
    Minismall.sequelize = new Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
        host: process.env.MYSQL_HOST,
        dialect: Minismall.config.sequelize.dialect,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });
    try {
        Minismall.sequelize.authenticate();
        console.log('Sequelize : Connection has been established successfully.');
    } catch (error) {
        console.error('Sequelize: Unable to connect to the database:', error);
    }
}


const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    name: 'sessiongreta',
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false
    } // à mettre à true uniquement avec un site https.
}));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
    done(null, user.id_users);
});

passport.deserializeUser(function(id, done) {
    const params_value = [];
    params_value.push(id);
    Minismall.sequelize.query("SELECT * FROM users WHERE id_users=?", {
        replacements: params_value,
        type: Minismall.sequelize.QueryTypes.SELECT
    }).then(function(result) { // sql query success
        console.log('deserialize user data : ', result);
        done(null, result);
    }).catch(function(err) { // sql query error
        console.log('error select', err);
    });

});

function setLocalStrategy() {
    // Version du code pour mongoDB via mongoose
    if (Minismall.config.minismall?.passport === "mongo") {
        return async function (username, password, done) {
            console.log("username : ", username, " password : ", password);
            try {
                const user = await Minismall.schemas["Users"].findOne({ login: username });
                if (!user) {
                    console.log("pas d'utilisateur trouvé");
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (user.mdp !== password) {
                    console.log("password erroné");
                    return done(null, false, { message: 'Incorrect password.' });
                }
                console.log("utilisateur : ", user);
                return done(null, user);
            } catch (err) {
                return done(err);
            }
        };
    }
    // ************************************************************************************** */
    // Version du code pour base SQL via Sequelize
    else if (Minismall.config.minismall?.passport === "mysql") {
        return async function (username, password, done) {
            try {
                const result = await Minismall.sequelize.query(
                    "SELECT id_users, login, mdp FROM users WHERE login=?",
                    {
                        replacements: [username],
                        type: Minismall.sequelize.QueryTypes.SELECT,
                    }
                );
                if (!result[0]) {
                    console.log("pas d'utilisateur trouvé");

                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (result[0].mdp !== password) {
                    console.log("password erroné");
                    return done(null, false, { message: 'Incorrect password.' });
                }
                console.log("utilisateur : ", result[0]);
                return done(null, result[0]);
            } catch (err) {
                console.log('error select', err);
                return done(err);
            }
        };
    } else {
        throw new Error("Stratégie de passport non définie dans le fichier de configuration");
    }
}

// Configuration de la stratégie Passport
passport.use(new LocalStrategy(setLocalStrategy()));


app.post('/authenticated', passport.authenticate('local'), function(req, res) {
    if (req.session.passport.user != null) {
        res.redirect('/index'); //le user est authentifié on affiche l’index il est en session
    } else {
        res.redirect('/'); // il n’est pas présent on renvoie à la boîte de login
    }
});

require('./dynamicRouter')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404);
    if (req.path.startsWith('/api')) {
        res.json({ error: 'Bad request or unauthorized' });
    } else {
        res.render('error', createError(404));
    }
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;


